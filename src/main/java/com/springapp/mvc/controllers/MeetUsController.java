package com.springapp.mvc.controllers;

import com.springapp.mvc.HelperClasses.*;
import com.springapp.mvc.Model.*;
import com.springapp.mvc.Services.*;
import javafx.beans.binding.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by vivek on 7/27/14.
 */
@Controller
public class MeetUsController {
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;
    @Autowired
    private VerificationService verificationService;
    @Autowired
    private MeetUpService meetUpService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private AutoSuggestService autoSuggestService;
    @Autowired
    private UserCommentService userCommentService;

    @RequestMapping(value = "/useradd", method = RequestMethod.POST)
    @ResponseBody
    public String userAdd(@RequestBody UserInfo userInfo1) {
        if (userInfo1 != null) {
            userService.addUser(userInfo1);
            verificationService.addVerificationString(userInfo1);
            verificationService.sendVerificationMessage(userInfo1);
            return "Success";
        }
        return "Failed";
    }

    @RequestMapping(value = "/checkRegistration", method = RequestMethod.POST)
    @ResponseBody
    public Boolean checkRegistrationDone(@RequestBody PhoneInfo phoneInfo) {
        if (phoneInfo != null) {
            userService.isVerficationDone(phoneInfo);
        }
        return false;
    }

    @RequestMapping(value = "/verifyUser", method = RequestMethod.POST)
    @ResponseBody
    public Tokens verifyUser(@RequestBody Verification verification) {
        if (verificationService.verifyVerificationString(verification)) {
            return authService.getAuthToken(verification);
        }
        return new Tokens();
    }

    @RequestMapping(value = "/verifyRegistration", method = RequestMethod.POST)
    @ResponseBody
    public RegistrationVerficationMessage verifyRegistration(@RequestBody UserInfo userInfo) {
        if (userInfo == null || userInfo.getPhoneInfo() == null) {
            return RegistrationVerficationMessage.getNegativeMessage(PhoneInfo.getPhoneInfo(0, 0L));
        }
        return userService.isUserRegisteredAndGenerateNewVerification(userInfo);
    }

    @RequestMapping(value = "/addMeetUp", method = RequestMethod.POST)
    @ResponseBody
    public String addMeetUp(@RequestBody StartMeetUpMessage startMeetUpMessage) {
        return meetUpService.setUpMeetUp(startMeetUpMessage);
    }

    @RequestMapping(value = "/invite", method = RequestMethod.POST)
    @ResponseBody
    public String invite(@RequestBody InviteMessage inviteMessage) {
        meetUpService.addInvitees(inviteMessage);
        return "";
    }

    @RequestMapping(value = "/verifyFriends", method = RequestMethod.POST)
    @ResponseBody
    public List<UserInfo> verifyFriends(@RequestBody UserAndContacts userAndContacts) {
        return contactService.verifyContacts(userAndContacts);
    }

    @RequestMapping(value = "/getUserMeetUps", method = RequestMethod.GET)
    @ResponseBody
    public List<MeetUpEntity> getUserMeetUps(@RequestParam(value = "countryCode") int countryCode, @RequestParam(value = "phoneNumber") long phoneNumber,
                                             @RequestParam(value = "pageNum") String pageNum) {
        UserMeetUpsRequest userMeetUpsRequest = new UserMeetUpsRequest();
        userMeetUpsRequest.setUserPhoneInfo(PhoneInfo.getPhoneInfo(countryCode, phoneNumber));
        userMeetUpsRequest.setPageNum(pageNum);
        return meetUpService.getUserMeetUps(userMeetUpsRequest);
    }

    @RequestMapping(value = "/getUserFeed", method = RequestMethod.GET)
    @ResponseBody
    public List<MeetUpEntity> getUserFeed(@RequestParam(value = "countryCode") int countryCode, @RequestParam(value = "phoneNumber") long phoneNumber,
                                          @RequestParam(value = "pageNum") String pageNum) {
        UserMeetUpsRequest userMeetUpsRequest = new UserMeetUpsRequest();
        userMeetUpsRequest.setUserPhoneInfo(PhoneInfo.getPhoneInfo(countryCode, phoneNumber));
        userMeetUpsRequest.setPageNum(pageNum);
        return meetUpService.getUserFeed(userMeetUpsRequest);
    }

    @RequestMapping(value = "/placesAutoSuggest", method = RequestMethod.GET)
    @ResponseBody
    public String placeAutoSuggest(@RequestParam(value = "query") String query) {
        return autoSuggestService.autoComplete(query);
    }

    @RequestMapping(value = "/getMeetUpComments", method = RequestMethod.GET)
    @ResponseBody
    public List<UserAndUserComment> getMeetUpComments(@RequestParam(value = "meetUpId") String meetUpId,
                                                      @RequestParam(value = "countryCode") int countryCode,
                                                      @RequestParam(value = "phoneNumber") long phoneNumber) {
        return userCommentService.getUserCommentsForMeetUp(meetUpId, countryCode, phoneNumber);
    }

    @RequestMapping(value = "/addUserComment", method = RequestMethod.POST)
    @ResponseBody
    private String addUserComment(@RequestBody UserComment userComment) {
        userCommentService.addUserComment(userComment);
        return "";
    }

    @RequestMapping(value = "/updateUserComment", method = RequestMethod.POST)
    @ResponseBody
    private String updateUserComment(@RequestBody UserComment userComment) {
        userCommentService.updateUserComment(userComment);
        return "";
    }

    @RequestMapping(value = "/removeUserComment", method = RequestMethod.POST)
    @ResponseBody
    private String removeUserComment(@RequestBody UserComment userComment) {
        userCommentService.deleteUserComment(userComment);
        return "";
    }

    @RequestMapping(value = "/updateMeetUp", method = RequestMethod.POST)
    @ResponseBody
    private String updateMeetUp(@RequestBody MeetUpEntityChanges changes) {
        meetUpService.updateMeetUp(changes);
        return "";
    }

    @RequestMapping(value = "/getMeetUpDetails", method = RequestMethod.GET)
    @ResponseBody
    public MeetUpPollResponse getMeetUpDetails(@RequestParam(value = "meetUpId") String meetUpId,
                                               @RequestParam(value = "countryCode") int countryCode,
                                               @RequestParam(value = "phoneNumber") long phoneNumber) {
        return meetUpService.getMeetUpPollResponse(meetUpId, countryCode, phoneNumber);
    }
}
