package com.springapp.mvc;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.beans.PropertyVetoException;

/**
 * Created by vivek on 7/13/14.
 */
@Configuration
@ComponentScan(basePackages = "com.springapp.mvc")
@PropertySource("classpath:application.properties")
@EnableWebMvc
@EnableTransactionManagement
public class AppConfig {
    @Bean
    public SessionFactory sessionFactory() throws PropertyVetoException {
        return new AnnotationConfiguration().configure().buildSessionFactory();
    }

}
