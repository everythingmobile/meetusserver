package com.springapp.mvc.data;

import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.Verification;
import org.springframework.stereotype.Repository;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Repository
public class VerificationRepository extends GenericCRUDRepository<Verification, PhoneInfo> {
    public VerificationRepository() {
        super(Verification.class);
    }
}
