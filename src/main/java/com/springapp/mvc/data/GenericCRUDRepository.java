package com.springapp.mvc.data;

import com.springapp.mvc.Util.Constants;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pradeep1 on 7/29/2014.
 */

public class GenericCRUDRepository<T, PK extends Serializable> {
    @Autowired
    SessionFactory sessionFactory;
    protected final Class<T> entityClass;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public GenericCRUDRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(T item) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(item);
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }

    public void saveOrUpdate(T item) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(item);
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }

    public void batchSave(List<T> items) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (int i = 0; i < items.size(); i++) {
                session.save(items.get(i));
                if (i % Constants.HIBERNATE_INSERT_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {

            session.close();
        }
    }

    public void batchSaveOrUpdate(List<T> items) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (int i = 0; i < items.size(); i++) {
                session.saveOrUpdate(items.get(i));
                if (i % Constants.HIBERNATE_INSERT_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }

    public void batchUpdate(List<T> items) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (int i = 0; i < items.size(); i++) {
                session.update(items.get(i));
                if (i % Constants.HIBERNATE_INSERT_BATCH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
            }
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }

    public void update(T item) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(item);
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }

    public T get(PK pk) {
        Session session = sessionFactory.openSession();
        try {
            T item = (T) session.get(entityClass, pk);
            return item;
        } catch (HibernateException h) {
            h.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public void delete(T item) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(item);
            transaction.commit();
        } catch (RuntimeException r) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }


}
