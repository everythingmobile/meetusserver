package com.springapp.mvc.data;

import com.springapp.mvc.Model.UserComment;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by pradeep1 on 1/3/2015.
 */
@Repository
public class UserCommentRepository extends GenericCRUDRepository<UserComment, Integer> {
    public UserCommentRepository() {
        super(UserComment.class);
    }

    public List<UserComment> getCommentsForMeetUp(String meeUpId) {
        Session session = sessionFactory.openSession();
        try {
            Query query = session.createQuery("from UserComment where meetUpId = (:meetUpId) order by commentTime asc");
            query.setParameter("meetUpId", meeUpId);
            return (List<UserComment>) query.list();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }
}
