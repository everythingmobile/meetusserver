package com.springapp.mvc.data;

import com.springapp.mvc.HelperClasses.UserAndMeetUp;
import com.springapp.mvc.Model.*;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Repository
public class MeetUpRepository extends GenericCRUDRepository<MeetUp, PhoneInfo> {
    public MeetUpRepository() {
        super(MeetUp.class);
    }

    public MeetUp getOneMeetUpRow(String meetUpId) {
        Session session = sessionFactory.openSession();

        try {
            Query query = session.createQuery("from MeetUp  where  meetUpId = (:meetUpId) ");
            query.setParameter("meetUpId", meetUpId);
            query.setMaxResults(1);
            return (MeetUp) query.list().get(0);
        } finally {
            session.close();
        }
    }

    public List<MeetUp> getAllMeetUpRows(String meetUpId) {
        Session session = sessionFactory.openSession();

        try {
            Query query = session.createQuery("from MeetUp  where  meetUpId = (:meetUpId) ");
            query.setParameter("meetUpId", meetUpId);
            return (List<MeetUp>) query.list();
        } finally {
            session.close();
        }
    }

    public void updateStartUpTime(String meetUpId, Date newStartUpTime) {
        List<MeetUp> meetUps = getAllMeetUpRows(meetUpId);

        for (MeetUp meetUp : meetUps) {
            meetUp.setStartUpTime(newStartUpTime);
        }
        if (meetUps.size() > 0) {
            batchUpdate(meetUps);
        }
    }

    public void removeUsersFromMeetUp(String meetUpId, List<UserInfo> users) {
        String hql = "delete from MeetUp meetUps where meetUps.meetUpId = '" + meetUpId + "' and meetUps.participantPhoneInfo.countryCode in (:ccs) and meetUps.participantPhoneInfo.phoneNumber in (:phones)";
        List<Long> phones = new ArrayList<>();
        List<Integer> ccs = new ArrayList<>();
        for (UserInfo userInfo : users) {
            phones.add(userInfo.getPhoneInfo().getPhoneNumber());
            ccs.add(userInfo.getPhoneInfo().getCountryCode());
        }

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            Query query = session.createQuery(hql).setParameterList("ccs", ccs).setParameterList("phones", phones);
            query.executeUpdate();
            transaction.commit();
        } finally {
            session.close();
        }
    }

    public void addUsersToMeetUp(String meetUpId, List<UserInfo> users) {
        MeetUp sample = getOneMeetUpRow(meetUpId);
        List<MeetUp> meetUps = new ArrayList<>();
        for (UserInfo userInfo : users) {
            MeetUp meetUp = new MeetUp();
            meetUp.setStartUpTime(sample.getStartUpTime());
            meetUp.setIsParticipantAdmin(false);
            meetUp.setMeetUpLocation(sample.getMeetUpLocation());
            meetUp.setMeetUpId(meetUpId);
            meetUp.setParticipantPhoneInfo(userInfo.getPhoneInfo());
            meetUps.add(meetUp);
        }
        if (meetUps.size() > 0) {
            batchSaveOrUpdate(meetUps);
        }
    }

    public boolean validateUserAndMeetUp(UserAndMeetUp userAndMeetUp) {
        Session session = sessionFactory.openSession();
        try {
            Query query = session.createQuery("from MeetUp where meetUpId = (:meetUpId) and participantPhoneInfo = (:phoneInfo)");
            query.setParameter("meetUpId", userAndMeetUp.getMeetUpId());
            query.setParameter("phoneInfo", userAndMeetUp.getPhoneInfo());
            return (query.list().isEmpty() == false);
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return false;
    }
}
