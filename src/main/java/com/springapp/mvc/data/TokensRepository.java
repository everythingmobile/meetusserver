package com.springapp.mvc.data;

import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.Tokens;
import com.springapp.mvc.Model.Verification;
import org.springframework.stereotype.Repository;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Repository
public class TokensRepository extends GenericCRUDRepository<Tokens, PhoneInfo> {
    public TokensRepository() {
        super(Tokens.class);
    }
}
