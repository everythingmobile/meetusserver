package com.springapp.mvc.data;

import com.springapp.mvc.Model.Friendship;
import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.Verification;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Repository
public class FriendshipRepository extends GenericCRUDRepository<Friendship, PhoneInfo> {
    public FriendshipRepository() {
        super(Friendship.class);
    }

    public List<PhoneInfo> getFriends(PhoneInfo userPhoneInfo) {
        Session session = sessionFactory.openSession();

        try {
            Query query = session.createQuery("select friendPhoneInfo from Friendship  where  userPhoneInfo = :userPhoneInfo");
            query.setParameter("userPhoneInfo", userPhoneInfo);
            List<PhoneInfo> friendPhoneInfo = (List<PhoneInfo>) query.list();

            return friendPhoneInfo;
        } finally {
            session.close();
        }
    }

}
