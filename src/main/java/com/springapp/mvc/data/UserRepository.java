package com.springapp.mvc.data;

import com.springapp.mvc.Model.MeetUp;
import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.UserInfo;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by vivek on 7/27/14.
 */
@Repository
public class UserRepository extends GenericCRUDRepository<UserInfo, PhoneInfo> {

    public UserRepository() {
        super(UserInfo.class);
    }

    public List<UserInfo> verifyRegisteredUsers(List<UserInfo> contacts) {

        if (contacts != null) {
            List<PhoneInfo> phoneInfos = new ArrayList<PhoneInfo>();

            for (UserInfo contact : contacts) {
                phoneInfos.add(contact.getPhoneInfo());
            }
            Session session = sessionFactory.openSession();

            try {
                Query query = session.createQuery("from UserInfo where phoneInfo in (:ids) and registered=true ");
                query.setParameterList("ids", phoneInfos);
                return (List<UserInfo>) query.list();
            } catch (HibernateException ex) {
                ex.printStackTrace();
            } finally {
                session.close();
            }
        }
        return null;
    }

    public List<UserInfo> getContacts(UserInfo userInfo) {
        Session session = sessionFactory.openSession();

        try {
            Query query = session.createQuery("from UserInfo users join Friendship friends on friends.userPhoneInfo= :userPhoneInfo and " +
                    "friends.friendPhoneInfo=users.phoneInfo and users.registered=true ");
            query.setParameter("userPhoneInfo", userInfo.getPhoneInfo());
            return (List<UserInfo>) query.list();
        } finally {
            session.close();
        }
    }


    public List<MeetUp> getUserMeetUps(PhoneInfo userPhoneInfo, int pageNum) {
        Session session = sessionFactory.openSession();
        int maxResultsPerPage = 10;

        try {
            Query query = session.createQuery("select distinct meetUpId from MeetUp where participantPhoneInfo= (:phoneInfo) " +
                    "order by abs(current_timestamp()-startUpTime)");
            query.setFirstResult((pageNum - 1) * maxResultsPerPage);
            query.setMaxResults(maxResultsPerPage);
            query.setParameter("phoneInfo", userPhoneInfo);
            List<String> meetUpIds = (List<String>) query.list();
            if (meetUpIds != null && !meetUpIds.isEmpty()) {
                Query query1 = session.createQuery("from MeetUp  where meetUpId in (:meetUpIds)");
                query1.setParameterList("meetUpIds", meetUpIds);
                List<MeetUp> meetUps = (List<MeetUp>) query1.list();
                return meetUps;
            }
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public List<MeetUp> getUserFeed(PhoneInfo userPhoneInfo, int pageNum) {
        Session session = sessionFactory.openSession();
        int maxResultsPerPage = 10;

        try {
            Query query = session.createSQLQuery("SELECT distinct meetUpId  from meetups meetup inner join friends friend " +
                    "on  meetup.participantPhoneNumber = friend.friendPhoneNumber " +
                    "and meetup.participantCountryCode = friend.friendCountryCode " +
                    "where friend.userPhoneNumber = (:userPhoneNumber) and friend.userCountryCode = (:userCountryCode) " +
                    "order by abs(current_timestamp() - meetup.startUpTime)");
            query.setFirstResult((pageNum - 1) * maxResultsPerPage);
            query.setMaxResults(maxResultsPerPage);
            query.setParameter("userPhoneNumber", userPhoneInfo.getPhoneNumber());
            query.setParameter("userCountryCode", userPhoneInfo.getCountryCode());
            List<String> meetUpIds = (List<String>) query.list();
            if (meetUpIds != null && !meetUpIds.isEmpty()) {
                Query query1 = session.createQuery("from MeetUp where meetUpId in (:meetUpIds)");
                query1.setParameterList("meetUpIds", meetUpIds);
                List<MeetUp> meetUps = (List<MeetUp>) query1.list();
                return meetUps;
            }
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }

    public List<UserInfo> getUsers(List<PhoneInfo> phoneInfos) {
        Session session = sessionFactory.openSession();

        try {
            Query query = session.createQuery("from UserInfo  where  phoneInfo in (:phoneInfos)");
            query.setParameterList("phoneInfos", phoneInfos);
            return (List<UserInfo>) query.list();
        } finally {
            session.close();
        }
    }
}
