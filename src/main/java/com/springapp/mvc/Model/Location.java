package com.springapp.mvc.Model;

import javax.persistence.Embeddable;

/**
 * Created by vivek on 11/19/14.
 */
@Embeddable
public class Location {
    private String destinationLatitude;
    private String destinationLongitude;
    private String locationDescription;
    private String locationAddress;

    public String getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(String destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public String getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(String destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public static Location getLocationObject(String destinationLatitude, String destinationLongitude, String locationDescription, String locationAddress) {
        Location location = new Location();
        location.setDestinationLatitude(destinationLatitude);
        location.setDestinationLongitude(destinationLongitude);
        location.setLocationDescription(locationDescription);
        location.setLocationAddress(locationAddress);
        return location;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }
}
