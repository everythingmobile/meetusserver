package com.springapp.mvc.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by pradeep1 on 7/15/2014.
 */
@Entity
@Table(name = "user_info")
public class UserInfo implements Serializable, Comparable {
    @Id
    private PhoneInfo phoneInfo;
    private String userName;
    private String emailId;
    private boolean registered;

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    @Override
    public String toString() {
        return Long.toString(getPhoneInfo().getCountryCode()) + Long.toString(getPhoneInfo().getPhoneNumber());
    }

    @Override
    public int compareTo(Object o) {
        UserInfo userInfo = (UserInfo) o;
        return this.getPhoneInfo().compareTo(userInfo.getPhoneInfo());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        UserInfo userInfo = (UserInfo) obj;
        if (userInfo.getPhoneInfo() == null || this.getPhoneInfo() == null) {
            return false;
        }
        return this.getPhoneInfo().getCountryCode() == userInfo.getPhoneInfo().getCountryCode() &&
                this.getPhoneInfo().getPhoneNumber() == userInfo.getPhoneInfo().getPhoneNumber();
    }
}
