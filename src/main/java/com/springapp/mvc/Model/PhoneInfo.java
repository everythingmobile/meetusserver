package com.springapp.mvc.Model;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by vivek on 11/16/14.
 */
@Embeddable
public class PhoneInfo implements Serializable, Comparable {
    private int countryCode;
    private long phoneNumber;

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static PhoneInfo getPhoneInfo(int countryCode, long phoneNumber) {
        PhoneInfo phoneInfo = new PhoneInfo();
        phoneInfo.setCountryCode(countryCode);
        phoneInfo.setPhoneNumber(phoneNumber);
        return phoneInfo;
    }

    @Override
    public int hashCode() {
        return countryCode + ((Long) phoneNumber).hashCode();
    }

    @Override
    public String toString() {
        return Integer.toString(this.getCountryCode()) + Long.toString(this.getPhoneNumber());
    }

    @Override
    public int compareTo(Object o) {
        PhoneInfo phoneInfo = (PhoneInfo) o;
        return this.toString().compareTo(phoneInfo.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        PhoneInfo phoneInfo = (PhoneInfo) obj;
        return this.countryCode == phoneInfo.getCountryCode() && this.phoneNumber == phoneInfo.getPhoneNumber();
    }
}
