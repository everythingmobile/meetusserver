package com.springapp.mvc.Model;

import javax.persistence.*;

/**
 * Created by vivek on 7/13/14.
 */
@Entity
@Table(name = "friends", indexes = {
        @Index(columnList = "userPhoneNumber, userCountryCode", name = "friends_phone_number_k")
}, uniqueConstraints = {
        @UniqueConstraint(columnNames = {"userCountryCode", "userPhoneNumber", "friendCountryCode", "friendPhoneNumber"})
})
public class Friendship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int tableId;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "countryCode", column = @Column(name = "userCountryCode")),
            @AttributeOverride(name = "phoneNumber", column = @Column(name = "userPhoneNumber"))
    })
    public PhoneInfo userPhoneInfo;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "countryCode", column = @Column(name = "friendCountryCode")),
            @AttributeOverride(name = "phoneNumber", column = @Column(name = "friendPhoneNumber"))
    })
    public PhoneInfo friendPhoneInfo;

    public PhoneInfo getUserPhoneInfo() {
        return userPhoneInfo;
    }

    public void setUserPhoneInfo(PhoneInfo userPhoneInfo) {
        this.userPhoneInfo = userPhoneInfo;
    }

    public PhoneInfo getFriendPhoneInfo() {
        return friendPhoneInfo;
    }

    public void setFriendPhoneInfo(PhoneInfo friendPhoneInfo) {
        this.friendPhoneInfo = friendPhoneInfo;
    }
}
