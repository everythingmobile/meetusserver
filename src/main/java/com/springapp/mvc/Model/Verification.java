package com.springapp.mvc.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by pradeep1 on 7/15/2014.
 */
@Entity
@Table(name = "verification_strings")
public class Verification {
    @Id
    private PhoneInfo phoneInfo;
    private String verificationString;

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getVerificationString() {
        return verificationString;
    }

    public void setVerificationString(String verificationString) {
        this.verificationString = verificationString;
    }
}
