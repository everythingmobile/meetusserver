package com.springapp.mvc.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vivek on 7/16/14.
 */
@Entity
@Table(name = "meetups", indexes = {
        @Index(columnList = "meetUpId", name = "meetups_meetupId_k"),
        @Index(columnList = "participantPhoneNumber, participantCountryCode", name = "meetups_participant_number_k")},
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"meetUpId", "participantCountryCode", "participantPhoneNumber"})
        })
public class MeetUp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int tableId;
    private String meetUpId;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "countryCode", column = @Column(name = "participantCountryCode")),
            @AttributeOverride(name = "phoneNumber", column = @Column(name = "participantPhoneNumber"))
    })
    private PhoneInfo participantPhoneInfo;
    private Date startUpTime;
    private Boolean isParticipantAdmin;
    @Embedded
    private Location meetUpLocation;

    public Location getMeetUpLocation() {
        return meetUpLocation;
    }

    public void setMeetUpLocation(Location meetUpLocation) {
        this.meetUpLocation = meetUpLocation;
    }

    public PhoneInfo getParticipantPhoneInfo() {
        return participantPhoneInfo;
    }

    public void setParticipantPhoneInfo(PhoneInfo participantPhoneInfo) {
        this.participantPhoneInfo = participantPhoneInfo;
    }

    public Date getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Date startUpTime) {
        this.startUpTime = startUpTime;
    }

    public Boolean getIsParticipantAdmin() {
        return isParticipantAdmin;
    }

    public void setIsParticipantAdmin(Boolean isParticipantAdmin) {
        this.isParticipantAdmin = isParticipantAdmin;
    }

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }
}
