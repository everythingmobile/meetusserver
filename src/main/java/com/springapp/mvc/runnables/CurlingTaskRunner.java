package com.springapp.mvc.runnables;

import com.springapp.mvc.HelperClasses.CurlTask;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by vivek on 5/22/15.
 */
public class CurlingTaskRunner implements Runnable {
    private LinkedBlockingQueue<CurlTask> curlTasksQueue;

    public void setCurlTasksQueue(LinkedBlockingQueue<CurlTask> curlTasksQueue) {
        this.curlTasksQueue = curlTasksQueue;
    }

    @Override
    public void run() {
        while (true) {
            CurlTask curlTask = null;
            try {
                curlTask = curlTasksQueue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (curlTask == null) {
                continue;
            }
            try {
                doCurlPost(curlTask.getUrl(), curlTask.getHeaders(), curlTask.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void doCurlPost(String url, Map<String, String> headers, String data) throws IOException {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            con.setRequestProperty(entry.getKey(), entry.getValue());
        }

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(data.getBytes());
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());
    }
}
