package com.springapp.mvc.exceptions;

/**
 * Created by vivek on 5/21/15.
 */
public class CurlTaskException extends Exception {
    private String message = null;

    public CurlTaskException() {
        super();
    }

    public CurlTaskException(String message) {
        super(message);
        this.message = message;
    }

    public CurlTaskException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
