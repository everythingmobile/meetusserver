package com.springapp.mvc.HelperClasses;

/**
 * Created by vivek on 2/1/15.
 */
public class UserMeetUpRelationConfig {
    public boolean isUserAdmin;
    public boolean isGoing;

    public UserMeetUpRelationConfig(boolean isUserAdmin, boolean isGoing) {
        this.isUserAdmin = isUserAdmin;
        this.isGoing = isGoing;
    }
}
