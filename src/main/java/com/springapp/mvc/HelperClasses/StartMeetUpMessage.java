package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.Location;
import com.springapp.mvc.Model.UserInfo;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 8/2/14.
 */
public class StartMeetUpMessage {
    private UserInfo admin;
    private Location meetUpLocation;
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    @JsonSerialize(using = JsonStdDateSerializer.class)
    private Date startUpTime;
    private List<UserInfo> invitees;

    public Date getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Date startUpTime) {
        this.startUpTime = startUpTime;
    }

    public UserInfo getAdmin() {
        return admin;
    }

    public void setAdmin(UserInfo admin) {
        this.admin = admin;
    }

    public Location getMeetUpLocation() {
        return meetUpLocation;
    }

    public void setMeetUpLocation(Location meetUpLocation) {
        this.meetUpLocation = meetUpLocation;
    }

    public List<UserInfo> getInvitees() {
        return invitees;
    }

    public void setInvitees(List<UserInfo> invitees) {
        this.invitees = invitees;
    }
}

