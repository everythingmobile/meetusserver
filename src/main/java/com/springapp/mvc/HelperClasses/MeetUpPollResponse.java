package com.springapp.mvc.HelperClasses;

/**
 * Created by vivek on 5/17/15.
 */
public class MeetUpPollResponse {
    private StartMeetUpMessage startMeetUpMessage;
    private UserMeetUpRelationConfig config;

    public MeetUpPollResponse(StartMeetUpMessage startMeetUpMessage, UserMeetUpRelationConfig config) {
        this.startMeetUpMessage = startMeetUpMessage;
        this.config = config;
    }

    public StartMeetUpMessage getStartMeetUpMessage() {
        return startMeetUpMessage;
    }

    public void setStartMeetUpMessage(StartMeetUpMessage startMeetUpMessage) {
        this.startMeetUpMessage = startMeetUpMessage;
    }

    public UserMeetUpRelationConfig getConfig() {
        return config;
    }

    public void setConfig(UserMeetUpRelationConfig config) {
        this.config = config;
    }
}
