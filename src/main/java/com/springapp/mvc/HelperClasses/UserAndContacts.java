package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.UserInfo;

import java.util.List;

/**
 * Created by pradeep1 on 8/2/2014.
 */
public class UserAndContacts {
    private UserInfo userInfo;
    private List<UserInfo> userContacts;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<UserInfo> getUserContacts() {
        return userContacts;
    }

    public void setUserContacts(List<UserInfo> userContacts) {
        this.userContacts = userContacts;
    }
}
