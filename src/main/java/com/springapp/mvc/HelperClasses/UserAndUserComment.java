package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.UserComment;
import com.springapp.mvc.Model.UserInfo;

/**
 * Created by pradeep1 on 2/1/2015.
 */
public class UserAndUserComment {
    private UserInfo userInfo;
    private UserComment userComment;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserComment getUserComment() {
        return userComment;
    }

    public void setUserComment(UserComment userComment) {
        this.userComment = userComment;
    }
}
