package com.springapp.mvc.HelperClasses;


import com.springapp.mvc.Model.PhoneInfo;

/**
 * Created by pradeep1 on 8/3/2014.
 */
public class UserMeetUpsRequest {

    private PhoneInfo userPhoneInfo;
    private String pageNum;

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public PhoneInfo getUserPhoneInfo() {
        return userPhoneInfo;
    }

    public void setUserPhoneInfo(PhoneInfo userPhoneInfo) {
        this.userPhoneInfo = userPhoneInfo;
    }
}
