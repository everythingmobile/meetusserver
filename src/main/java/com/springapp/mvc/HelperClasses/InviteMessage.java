package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.UserInfo;

import java.util.List;

/**
 * Created by vivek on 8/2/14.
 */
public class InviteMessage
{
    private String meetUpId;
    private List<UserInfo> invitees;
    private UserInfo inviter;

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    public List<UserInfo> getInvitees() {
        return invitees;
    }

    public void setInvitees(List<UserInfo> invitees) {
        this.invitees = invitees;
    }

    public UserInfo getInviter() {
        return inviter;
    }

    public void setInviter(UserInfo inviter) {
        this.inviter = inviter;
    }
}
