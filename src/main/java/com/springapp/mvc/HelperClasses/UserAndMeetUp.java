package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.PhoneInfo;

/**
 * Created by pradeep1 on 1/3/2015.
 */
public class UserAndMeetUp {
    private PhoneInfo phoneInfo;
    private String meetUpId;

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }
}
