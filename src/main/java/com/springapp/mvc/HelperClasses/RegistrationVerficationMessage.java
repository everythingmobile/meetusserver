package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.PhoneInfo;

/**
 * Created by vivek on 9/13/14.
 */
public class RegistrationVerficationMessage {
    private PhoneInfo phoneInfo;
    private Boolean registered;

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    public static RegistrationVerficationMessage getPositiveMessage(PhoneInfo phoneInfo) {
        RegistrationVerficationMessage registrationVerficationMessage = new RegistrationVerficationMessage();
        registrationVerficationMessage.setPhoneInfo(phoneInfo);
        registrationVerficationMessage.setRegistered(true);
        return registrationVerficationMessage;
    }

    public static RegistrationVerficationMessage getNegativeMessage(PhoneInfo phoneInfo) {
        RegistrationVerficationMessage registrationVerficationMessage = new RegistrationVerficationMessage();
        registrationVerficationMessage.setPhoneInfo(phoneInfo);
        registrationVerficationMessage.setRegistered(false);
        return registrationVerficationMessage;
    }
}
