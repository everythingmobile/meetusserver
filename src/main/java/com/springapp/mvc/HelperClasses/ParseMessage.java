package com.springapp.mvc.HelperClasses;

/**
 * Created by vivek on 5/22/15.
 */
public class ParseMessage {
    private String where;
    private String alert;
    private String meetUpId;

    public ParseMessage(String where, String alert, String meetUpId) {
        this.where = where;
        this.alert = alert;
        this.meetUpId = meetUpId;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    @Override
    public String toString() {
        return "{\"where\":{\"sharedPreferencesParsePhone\": \"" + where
                + "\"},\"data\":{\"alert\": \""+ alert +"\", \"meetUpId\":\""+ meetUpId +"\"}}";
    }
}
