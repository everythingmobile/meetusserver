package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.Model.Location;
import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.UserInfo;

import java.util.Date;
import java.util.List;

/**
 * Created by Pravin Gadakh on 8/9/14.
 */
public class MeetUpEntity {

    private PhoneInfo adminPhoneInfo;
    private Location meetUpLocation;
    private String meetUpId;
    private List<Integer> friends;
    private List<UserInfo> participants;
    private Date startUpTime;

    public PhoneInfo getAdminPhoneInfo() {
        return adminPhoneInfo;
    }

    public void setAdminPhoneInfo(PhoneInfo adminPhoneInfo) {
        this.adminPhoneInfo = adminPhoneInfo;
    }

    public Date getStartUpTime() {
        return startUpTime;
    }

    public void setStartUpTime(Date startUpTime) {
        this.startUpTime = startUpTime;
    }

    public Location getMeetUpLocation() {
        return meetUpLocation;
    }

    public void setMeetUpLocation(Location meetUpLocation) {
        this.meetUpLocation = meetUpLocation;
    }

    public String getMeetUpId() {
        return meetUpId;
    }

    public void setMeetUpId(String meetUpId) {
        this.meetUpId = meetUpId;
    }

    public List<UserInfo> getParticipants() {
        return participants;
    }

    public void setParticipants(List<UserInfo> participants) {
        this.participants = participants;
    }

    public List<Integer> getFriends() {
        return friends;
    }

    public void setFriends(List<Integer> friends) {
        this.friends = friends;
    }
}
