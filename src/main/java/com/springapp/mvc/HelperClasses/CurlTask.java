package com.springapp.mvc.HelperClasses;

import java.util.Map;

/**
 * Created by vivek on 5/23/15.
 */
public class CurlTask {
    private String url;
    private Map<String, String> headers;
    private String method;
    private String data;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
