package com.springapp.mvc.HelperClasses;

import com.springapp.mvc.exceptions.CurlTaskException;

import java.util.Map;

/**
 * Created by vivek on 5/23/15.
 */
public class CurlTaskBuilder {
    private CurlTask curlTask;

    private CurlTask getCurlTaskInstance() {
        if (curlTask == null) {
            curlTask = new CurlTask();
        }
        return curlTask;
    }

    public CurlTaskBuilder setUrl(String url) {
        getCurlTaskInstance().setUrl(url);
        return this;
    }

    public CurlTaskBuilder setHeaders(Map<String, String> headers) {
        getCurlTaskInstance().setHeaders(headers);
        return this;
    }

    public CurlTaskBuilder setData(String data) {
        getCurlTaskInstance().setData(data);
        return this;
    }

    public CurlTaskBuilder setMethod(String method) {
        getCurlTaskInstance().setMethod(method);
        return this;
    }

    public CurlTask build() throws CurlTaskException {
        CurlTask currentTask = getCurlTaskInstance();
        if (currentTask.getMethod() == null || currentTask.getMethod().equals("")) {
            throw new CurlTaskException("No method");
        }
        if (currentTask.getHeaders().size() == 0) {
            throw new CurlTaskException("No headers");
        }
        if (currentTask.getUrl() == null || currentTask.getUrl().equals("")) {
            throw new CurlTaskException(currentTask.getUrl() + " is not a valid Url");
        }
        return currentTask;
    }
}
