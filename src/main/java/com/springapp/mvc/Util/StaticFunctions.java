package com.springapp.mvc.Util;

import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.UserInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by vivek on 8/2/14.
 */
public class StaticFunctions {

    public static String generateMD5Hash(String rawString) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            System.out.println(rawString);
            md.update(rawString.getBytes());
            byte[] hash = md.digest();
            StringBuilder stringBuilder = new StringBuilder();
            //converting byte array to hexadecimal format
            for (byte b : hash) {
                stringBuilder.append(String.format("%02x", b & 0xff));
            }
            System.out.println(stringBuilder.toString());
            return stringBuilder.toString();

        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> union(List<T> list1, List<T> list2) {
        Set<T> set = new HashSet<T>();

        set.addAll(list1);
        set.addAll(list2);

        return new ArrayList<T>(set);
    }

    public static <T> List<T> intersection(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<T>();

        for (T t : list1) {
            if (list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

    public static String doCurl(String finalUrl) {
        String response = "";
        try {
            URL url = new URL(finalUrl);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            for (String line; (line = reader.readLine()) != null; ) {
                response += line;
            }
        } catch (MalformedURLException e) {
            return "";
        } catch (IOException e) {
            return "";
        }
        return response;
    }

    public static <T extends Comparable> ArrayList<Integer> intersectionWithIndex(List<T> firstArray, List<T> secondArray) {
        int i = 0;
        int j = 0;
        ArrayList<Integer> list = new ArrayList<Integer>();
        while (i < firstArray.size() && j < secondArray.size()) {
            if (firstArray.get(i).compareTo(secondArray.get(j)) < 0)
                i++;// Increase I move to next element
            else if (secondArray.get(j).compareTo(firstArray.get(i)) < 0)
                j++;// Increase J move to next element
            else {
                list.add(i++);
                j++;// If same increase I & J both
            }
        }
        return list;
    }

    public static List<PhoneInfo> getPhoneInfosFromUserInfos(List<UserInfo> userInfos) {
        List<PhoneInfo> phoneInfos = new ArrayList<PhoneInfo>();
        for (UserInfo userInfo : userInfos) {
            phoneInfos.add(userInfo.getPhoneInfo());
        }
        return phoneInfos;
    }
}
