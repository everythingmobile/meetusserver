package com.springapp.mvc.Services;

import com.springapp.mvc.HelperClasses.CurlTask;
import com.springapp.mvc.HelperClasses.CurlTaskBuilder;
import com.springapp.mvc.HelperClasses.ParseMessage;
import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.exceptions.CurlTaskException;
import com.springapp.mvc.runnables.CurlingTaskRunner;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by vivek on 5/5/15.
 */
@Service
public class ParseService {
    public static final String MEETUP_ADDED_ALERT = "You were added to a Meet Up";
    public static final String COMMENT_SUFFIX = " commented on a Meet Up";
    public static final String PARSE_PUSH_URL = "https://api.parse.com/1/push";
    private LinkedBlockingQueue<CurlTask> curlTasksQueue = new LinkedBlockingQueue<>(100);
    private Map<String, String> headers = new HashMap<>();

    @PostConstruct
    private void initCurlTaskRunner() {
        CurlingTaskRunner curlingTaskRunner = new CurlingTaskRunner();
        curlingTaskRunner.setCurlTasksQueue(curlTasksQueue);
        new Thread(curlingTaskRunner).start();
    }

    @PostConstruct
    private void setHeaders() {
        headers.put("X-Parse-Application-Id", "S1FwHCdcx5f61HetltjRKZF3eKH5GOxuImLLMGOH");
        headers.put("X-Parse-REST-API-Key", "MQh83xA8F71DOjWJnbDKnlRF1e61zJJR7CZgknTw");
        headers.put("Content-Type", "application/json");
    }

    public void sendMeetUpAddedNotifications(List<UserInfo> userInfos, String meetUpId) {
        for (UserInfo userInfo : userInfos) {
            addMessageToQueue(meetUpId, userInfo, MEETUP_ADDED_ALERT);
        }
    }

    public void sendCommentAdded(List<UserInfo> userInfos, String meetUpId, UserInfo commentor) {
        for (UserInfo userInfo : userInfos) {
            addMessageToQueue(meetUpId, userInfo, commentor.getUserName() + COMMENT_SUFFIX);
        }
    }

    private void addMessageToQueue(String meetUpId, UserInfo userInfo, String alert) {
        ParseMessage parseMessage = new ParseMessage(Integer.toString(userInfo.getPhoneInfo().getCountryCode()) + "_" + Long.toString(userInfo.getPhoneInfo().getPhoneNumber()),
                alert, meetUpId);
        CurlTask curlTask = null;
        try {
            curlTask = new CurlTaskBuilder().setUrl(PARSE_PUSH_URL).setData(parseMessage.toString())
                    .setHeaders(headers).setMethod("POST").build();
        } catch (CurlTaskException e) {
            e.printStackTrace();
        }
        if (curlTask == null) {
            return;
        }
        curlTasksQueue.add(curlTask);
    }
}
