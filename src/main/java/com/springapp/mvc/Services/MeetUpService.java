package com.springapp.mvc.Services;

import com.springapp.mvc.HelperClasses.*;
import com.springapp.mvc.Model.*;
import com.springapp.mvc.Util.StaticFunctions;
import com.springapp.mvc.data.FriendshipRepository;
import com.springapp.mvc.data.MeetUpRepository;
import com.springapp.mvc.data.UserCommentRepository;
import com.springapp.mvc.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by vivek on 8/2/14.
 */
@Service
public class MeetUpService {
    @Autowired
    private MeetUpRepository meetUpRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;
    @Autowired
    private UserCommentRepository userCommentRepository;
    @Autowired
    private ParseService parseService;

    public String setUpMeetUp(StartMeetUpMessage startMeetUpMessage) {
        //TODO: Check invitees before inviting
        String meetUpId = getMeetUpIdFromMessage(startMeetUpMessage);

        int currentUserIndex = startMeetUpMessage.getInvitees().indexOf(startMeetUpMessage.getAdmin());
        if (currentUserIndex != -1) {
            startMeetUpMessage.getInvitees().remove(currentUserIndex);
        }

        saveMeetUps(meetUpId, startMeetUpMessage);
        startMeetUpMessage.getInvitees().remove(startMeetUpMessage.getAdmin());
        parseService.sendMeetUpAddedNotifications(startMeetUpMessage.getInvitees(), meetUpId);

        return meetUpId;
    }

    private void saveMeetUps(String meetUpId, StartMeetUpMessage startMeetUpMessage) {
        List<MeetUp> finalList;

        MeetUp meetUpForAdmin = new MeetUp();
        meetUpForAdmin.setMeetUpId(meetUpId);
        meetUpForAdmin.setMeetUpLocation(startMeetUpMessage.getMeetUpLocation());
        meetUpForAdmin.setIsParticipantAdmin(true);
        meetUpForAdmin.setParticipantPhoneInfo(startMeetUpMessage.getAdmin().getPhoneInfo());
        meetUpForAdmin.setStartUpTime(startMeetUpMessage.getStartUpTime());

        finalList = getMeetUpObjects(meetUpId, startMeetUpMessage.getInvitees(), startMeetUpMessage.getMeetUpLocation(), startMeetUpMessage.getStartUpTime());
        finalList.add(meetUpForAdmin);

        meetUpRepository.batchSave(finalList);
    }

    private List<MeetUp> getMeetUpObjects(String meetUpId, List<UserInfo> invitees, Location meetUpLocation, Date startUpTime) {
        List<MeetUp> finalList = new ArrayList<>();
        for (UserInfo user : invitees) {
            MeetUp meetUp = new MeetUp();
            meetUp.setMeetUpId(meetUpId);
            meetUp.setMeetUpLocation(meetUpLocation);
            meetUp.setParticipantPhoneInfo(user.getPhoneInfo());
            meetUp.setIsParticipantAdmin(false);
            meetUp.setStartUpTime(startUpTime);
            finalList.add(meetUp);
        }
        return finalList;
    }

    public void addInvitees(InviteMessage inviteMessage) {
        removeNonUserFromInvitees(inviteMessage);
        MeetUp currentMeetUp = meetUpRepository.getOneMeetUpRow(inviteMessage.getMeetUpId());
        List<MeetUp> finalList = getMeetUpObjects(inviteMessage.getMeetUpId(), inviteMessage.getInvitees(), currentMeetUp.getMeetUpLocation(), currentMeetUp.getStartUpTime());
        meetUpRepository.batchSave(finalList);
    }

    private void removeNonUserFromInvitees(InviteMessage inviteMessage) {
        inviteMessage.setInvitees(userRepository.getUsers(StaticFunctions.getPhoneInfosFromUserInfos(inviteMessage.getInvitees())));
    }

    private String getMeetUpIdFromMessage(StartMeetUpMessage startMeetUpMessage) {
        return StaticFunctions.generateMD5Hash(startMeetUpMessage.getAdmin().getPhoneInfo().toString() + Long.toString(new Date().getTime()));
    }

    public List<MeetUpEntity> getUserMeetUps(UserMeetUpsRequest userMeetUpsRequest) {
        try {
            if (userMeetUpsRequest != null && userMeetUpsRequest.getPageNum() != null) {
                List<MeetUp> meetUps = userRepository.getUserMeetUps(userMeetUpsRequest.getUserPhoneInfo(), Integer.parseInt(userMeetUpsRequest.getPageNum()));
                if (meetUps != null && !meetUps.isEmpty()) {
                    return createUserFeedList(meetUps, userMeetUpsRequest.getUserPhoneInfo());
                }
            }
        } catch (NumberFormatException nx) {
            nx.printStackTrace();
        }
        return null;
    }

    public List<MeetUpEntity> getUserFeed(UserMeetUpsRequest userMeetUpsRequest) {
        try {
            if (userMeetUpsRequest != null && userMeetUpsRequest.getPageNum() != null) {
                List<MeetUp> meetUps = userRepository.getUserFeed(userMeetUpsRequest.getUserPhoneInfo(), Integer.parseInt(userMeetUpsRequest.getPageNum()));
                if (meetUps != null && !meetUps.isEmpty()) {
                    return createUserFeedList(meetUps, userMeetUpsRequest.getUserPhoneInfo());
                }
            }
        } catch (NumberFormatException nx) {
            nx.printStackTrace();
        }
        return null;
    }

    private List<MeetUpEntity> createUserFeedList(List<MeetUp> meetUps, PhoneInfo userPhoneInfo) {

        Map<String, MeetUpEntity> meetUpFeedMap = new HashMap<>();
        Map<String, List<PhoneInfo>> meetUpParticipants = new HashMap<>();
        Map<String, List<Integer>> friends = new HashMap<>();

        for (MeetUp meetUp : meetUps) {

            String meetUpId = meetUp.getMeetUpId();
            MeetUpEntity meetUpEntity = meetUpFeedMap.get(meetUpId);

            if (meetUpEntity == null) {
                meetUpEntity = new MeetUpEntity();
                meetUpEntity.setMeetUpId(meetUpId);
                meetUpEntity.setMeetUpLocation(meetUp.getMeetUpLocation());
                meetUpEntity.setParticipants(new ArrayList<UserInfo>());
                meetUpEntity.setFriends(new ArrayList<Integer>());
                meetUpEntity.setStartUpTime(meetUp.getStartUpTime());
                meetUpFeedMap.put(meetUpId, meetUpEntity);

                meetUpParticipants.put(meetUpId, new ArrayList<PhoneInfo>());
                friends.put(meetUpId, new ArrayList<Integer>());
            }
            if (meetUp.getIsParticipantAdmin()) {
                meetUpEntity.setAdminPhoneInfo(meetUp.getParticipantPhoneInfo());
            }
            meetUpParticipants.get(meetUpId).add(meetUp.getParticipantPhoneInfo());

        }
        for (String meetUpId : meetUpParticipants.keySet()) {
            List<PhoneInfo> participantsList = meetUpParticipants.get(meetUpId);
            if (!participantsList.isEmpty()) {
                List<UserInfo> meetUpUsers = userRepository.getUsers(participantsList);
                meetUpFeedMap.get(meetUpId).getParticipants().addAll(meetUpUsers);
            }
        }

        List<PhoneInfo> friendPhoneNumbers = friendshipRepository.getFriends(userPhoneInfo);
        Collections.sort(friendPhoneNumbers);

        for (String meetUpId : meetUpFeedMap.keySet()) {
            List<PhoneInfo> participants = StaticFunctions.getPhoneInfosFromUserInfos(meetUpFeedMap.get(meetUpId).getParticipants());
            Collections.sort(participants);
            friends.get(meetUpId).addAll(StaticFunctions.intersectionWithIndex(participants, friendPhoneNumbers));
        }

        for (String meetUpId : friends.keySet()) {
            meetUpFeedMap.get(meetUpId).getFriends().addAll(friends.get(meetUpId));
        }

        return new ArrayList<>(meetUpFeedMap.values());
    }

    public void updateMeetUp(MeetUpEntityChanges changes) {
        checkAndToRespondToRequest(changes);
    }

    private void checkAndToRespondToRequest(MeetUpEntityChanges changes) {
        if (changes.getMeetUpId() == null || changes.getMeetUpId().equals("")) {
            return;
        }
        if (changes.getStartUpTime() != null) {
            updateStartUpTime(changes);
        }
        if (changes.getAddedUsers() != null && changes.getAddedUsers().size() > 0) {
            updateAddedUsers(changes);
        }
        if (changes.getRemovedUsers() != null && changes.getRemovedUsers().size() > 0) {
            updateRemovedUsers(changes);
        }

    }

    private void updateStartUpTime(MeetUpEntityChanges changes) {
        meetUpRepository.updateStartUpTime(changes.getMeetUpId(), changes.getStartUpTime());
    }

    private void updateRemovedUsers(MeetUpEntityChanges changes) {
        meetUpRepository.removeUsersFromMeetUp(changes.getMeetUpId(), changes.getRemovedUsers());
    }

    private void updateAddedUsers(MeetUpEntityChanges changes) {
        meetUpRepository.addUsersToMeetUp(changes.getMeetUpId(), changes.getAddedUsers());
    }

    public MeetUpPollResponse getMeetUpPollResponse(String meetUpId, int countryCode, long phoneNumber) {
        PhoneInfo currentPhoneInfo = new PhoneInfo();
        currentPhoneInfo.setCountryCode(countryCode);
        currentPhoneInfo.setPhoneNumber(phoneNumber);

        StartMeetUpMessage startMeetUpMessage = new StartMeetUpMessage();
        boolean isUserGoing = false, isUserAdmin = false;

        List<MeetUp> meetUps = meetUpRepository.getAllMeetUpRows(meetUpId);
        List<PhoneInfo> phoneInfosOfInvitees = new ArrayList<>();
        for (MeetUp meetUp : meetUps) {
            phoneInfosOfInvitees.add(meetUp.getParticipantPhoneInfo());
        }
        if (phoneInfosOfInvitees.size() == 0) {
            return null;
        }
        startMeetUpMessage.setInvitees(userRepository.getUsers(phoneInfosOfInvitees));
        startMeetUpMessage.setMeetUpLocation(meetUps.get(0).getMeetUpLocation());
        startMeetUpMessage.setStartUpTime(meetUps.get(0).getStartUpTime());
        for (int i = 0; i < meetUps.size(); i++) {
            isUserGoing = meetUps.get(i).getParticipantPhoneInfo().equals(currentPhoneInfo) || isUserGoing;
            if (meetUps.get(i).getIsParticipantAdmin()) {
                isUserAdmin = meetUps.get(i).getParticipantPhoneInfo().equals(currentPhoneInfo);
                startMeetUpMessage.setAdmin(startMeetUpMessage.getInvitees().get(i));
            }
        }

        return new MeetUpPollResponse(startMeetUpMessage,
                new UserMeetUpRelationConfigBuilder().setIsGoing(isUserGoing).setIsUserAdmin(isUserAdmin).build());
    }
}
