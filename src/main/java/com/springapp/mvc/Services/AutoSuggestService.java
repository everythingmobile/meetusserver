package com.springapp.mvc.Services;

import com.springapp.mvc.Util.StaticFunctions;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by vivek on 11/16/14.
 */
@Service
public class AutoSuggestService {
    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
    private static final String API_KEY = "AIzaSyDrDYbf-o203niPeY9_vLXPr2BBww-5RMQ";

    public String autoComplete(String query) {
        String finalUrl;
        try {
            finalUrl = BASE_URL + "?key=" + API_KEY + "&input=" + URLEncoder.encode(query, "utf8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
        return StaticFunctions.doCurl(finalUrl);
    }
}
