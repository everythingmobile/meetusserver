package com.springapp.mvc.Services;

import com.springapp.mvc.Model.Tokens;
import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.Model.Verification;
import com.springapp.mvc.Util.StaticFunctions;
import com.springapp.mvc.data.TokensRepository;
import com.springapp.mvc.data.UserRepository;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Service
public class AuthService {
    @Autowired
    private TokensRepository tokensRepository;
    @Autowired
    private UserRepository userRepository;


    public Tokens getAuthToken(Verification verification) {
        Tokens authTokenInfo = tokensRepository.get(verification.getPhoneInfo());
        if (authTokenInfo == null) {
            UserInfo user = userRepository.get(verification.getPhoneInfo());
            if (user != null) {
                String token = StaticFunctions.generateMD5Hash(user.getPhoneInfo().toString() + user.getUserName() + Long.toString(new Date().getTime()));
                authTokenInfo = new Tokens();
                authTokenInfo.setPhoneInfo(user.getPhoneInfo());
                authTokenInfo.setAuthToken(token);
                authTokenInfo.setUserName(user.getUserName());
                tokensRepository.save(authTokenInfo);
                return authTokenInfo;
            } else {
                return null;
            }
        } else {
            UserInfo user = userRepository.get(verification.getPhoneInfo());
            if (user != null) {
                return authTokenInfo;
            }
        }

        return null;
    }

    public void refreshAuthToken(UserInfo userInfo) {
        Tokens authTokenInfo = tokensRepository.get(userInfo.getPhoneInfo());
        UserInfo user = userRepository.get(userInfo.getPhoneInfo());
        if (authTokenInfo != null && user != null) {
            authTokenInfo.setAuthToken(StaticFunctions.generateMD5Hash(user.getPhoneInfo().toString() + user.getUserName() + Long.toString(new Date().getTime())));
        }
        tokensRepository.update(authTokenInfo);
    }

    public boolean verifyAuthToken(Tokens authToken) {
        if (authToken != null) {
            Tokens authToken1 = tokensRepository.get(authToken.getPhoneInfo());
            return (authToken1 != null && authToken1.getAuthToken().equals(authToken.getAuthToken()));
        }
        return false;
    }
}
