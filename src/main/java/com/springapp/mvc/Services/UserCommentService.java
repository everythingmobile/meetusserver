package com.springapp.mvc.Services;

import com.springapp.mvc.HelperClasses.MeetUpPollResponse;
import com.springapp.mvc.HelperClasses.UserAndMeetUp;
import com.springapp.mvc.HelperClasses.UserAndUserComment;
import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.UserComment;
import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.data.MeetUpRepository;
import com.springapp.mvc.data.UserCommentRepository;
import com.springapp.mvc.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pradeep1 on 1/3/2015.
 */
@Service
public class UserCommentService {
    @Autowired
    private UserCommentRepository userCommentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MeetUpRepository meetUpRepository;
    @Autowired
    private MeetUpService meetUpService;
    @Autowired
    private ParseService parseService;

    private UserAndMeetUp getUserAndMeetUp(UserComment userComment) {
        UserAndMeetUp userAndMeetUp = new UserAndMeetUp();
        userAndMeetUp.setMeetUpId(userComment.getMeetUpId());
        userAndMeetUp.setPhoneInfo(userComment.getPhoneInfo());
        return userAndMeetUp;
    }

    private boolean isValidUser(PhoneInfo phoneInfo) {
        UserInfo userInfo = userRepository.get(phoneInfo);
        return userInfo != null;
    }

    private boolean isValidComment(UserComment userComment) {
        return userComment.getCommentId() != 0;
    }

    public void addUserComment(UserComment userComment) {
        if (meetUpRepository.validateUserAndMeetUp(getUserAndMeetUp(userComment)) && isValidUser(userComment.getPhoneInfo())) {
            userCommentRepository.save(userComment);
        }
        sendPushNotifications(userComment, userComment.getMeetUpId());
    }

    private void sendPushNotifications(UserComment userComment, String meetUpId) {
        MeetUpPollResponse meetUpPollResponse = meetUpService.getMeetUpPollResponse(userComment.getMeetUpId(), userComment.getPhoneInfo().getCountryCode(), userComment.getPhoneInfo().getPhoneNumber());
        UserInfo commentor = removeCurrentUser(userComment.getPhoneInfo(), meetUpPollResponse.getStartMeetUpMessage().getInvitees());
        if (commentor == null) {
            return;
        }
        parseService.sendCommentAdded(meetUpPollResponse.getStartMeetUpMessage().getInvitees(), meetUpId, commentor);
    }

    private UserInfo removeCurrentUser(PhoneInfo phoneInfo, List<UserInfo> userInfos) {
        for (int i = 0; i < userInfos.size(); i++) {
            UserInfo userInfo = userInfos.get(i);
            if (userInfo.getPhoneInfo().equals(phoneInfo)) {
                return userInfos.remove(i);
            }
        }
        return null;
    }

    public void updateUserComment(UserComment userComment) {
        UserComment userComment1 = userCommentRepository.get(userComment.getCommentId());
        if (userComment.equals(userComment1) && isValidComment(userComment) && isValidUser(userComment.getPhoneInfo()) && meetUpRepository.validateUserAndMeetUp(getUserAndMeetUp(userComment))) {
            userCommentRepository.update(userComment);
        }
    }

    public void deleteUserComment(UserComment userComment) {
        UserComment userComment1 = userCommentRepository.get(userComment.getCommentId());
        if (userComment.equals(userComment1) && isValidComment(userComment) && isValidUser(userComment.getPhoneInfo()) && meetUpRepository.validateUserAndMeetUp(getUserAndMeetUp(userComment))) {
            userCommentRepository.delete(userComment);
        }
    }

    public List<UserAndUserComment> getUserCommentsForMeetUp(String meetUpId, int countryCode, long phoneNumber) {
        PhoneInfo phoneInfo = PhoneInfo.getPhoneInfo(countryCode, phoneNumber);
        UserAndMeetUp userAndMeetUp = new UserAndMeetUp();
        userAndMeetUp.setMeetUpId(meetUpId);
        userAndMeetUp.setPhoneInfo(phoneInfo);
        List<UserComment> userComments = new ArrayList<UserComment>();
        if (isValidUser(phoneInfo) && meetUpRepository.validateUserAndMeetUp(userAndMeetUp)) {
            userComments = userCommentRepository.getCommentsForMeetUp(meetUpId);
            return getUserAndCommentList(userComments);
        }
        return null;
    }

    private Map<PhoneInfo, UserInfo> mapPhoneInfoToUserInfo(List<PhoneInfo> phoneInfoList) {
        Map<PhoneInfo, UserInfo> phoneInfoUserInfoMap = new HashMap<PhoneInfo, UserInfo>();
        List<UserInfo> userInfos = userRepository.getUsers(phoneInfoList);
        for (UserInfo userInfo : userInfos) {
            phoneInfoUserInfoMap.put(userInfo.getPhoneInfo(), userInfo);
        }
        return phoneInfoUserInfoMap;
    }

    private List<UserAndUserComment> getUserAndCommentList(List<UserComment> userComments) {
        List<UserAndUserComment> userAndUserCommentList = new ArrayList<UserAndUserComment>();
        List<PhoneInfo> phoneInfos = new ArrayList<PhoneInfo>();
        for (UserComment userComment : userComments) {
            phoneInfos.add(userComment.getPhoneInfo());
        }
        Map<PhoneInfo, UserInfo> phoneInfoUserInfoMap = mapPhoneInfoToUserInfo(phoneInfos);
        for (UserComment userComment : userComments) {
            UserAndUserComment userAndUserComment = new UserAndUserComment();
            userAndUserComment.setUserComment(userComment);
            userAndUserComment.setUserInfo(phoneInfoUserInfoMap.get(userComment.getPhoneInfo()));
            userAndUserCommentList.add(userAndUserComment);
        }
        return userAndUserCommentList;
    }
}
