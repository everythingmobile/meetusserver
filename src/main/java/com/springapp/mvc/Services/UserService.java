package com.springapp.mvc.Services;

import com.springapp.mvc.HelperClasses.RegistrationVerficationMessage;
import com.springapp.mvc.Model.PhoneInfo;
import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VerificationService verificationService;

    public void addUser(UserInfo userInfo)
    {
        userInfo.setRegistered(true);
        userRepository.saveOrUpdate(userInfo);
    }

    public UserInfo getUser(PhoneInfo phoneInfo)
    {
        return userRepository.get(phoneInfo);
    }


    public RegistrationVerficationMessage isUserRegisteredAndGenerateNewVerification(UserInfo userInfo) {
        UserInfo userInTheDB = getUser(userInfo.getPhoneInfo());
        if(userInTheDB == null || !userInTheDB.isRegistered()) {
            return RegistrationVerficationMessage.getNegativeMessage(userInfo.getPhoneInfo());
        }
        userInTheDB.setRegistered(true);
        verificationService.addVerificationString(userInTheDB);
        return RegistrationVerficationMessage.getPositiveMessage(userInfo.getPhoneInfo());
    }

    public boolean isVerficationDone(PhoneInfo phoneInfo) {
        UserInfo userInfo = userRepository.get(phoneInfo);
        if (userInfo == null) {
            return false;
        }
        return userInfo.isRegistered();
    }
}
