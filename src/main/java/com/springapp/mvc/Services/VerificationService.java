package com.springapp.mvc.Services;

import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.Model.Verification;
import com.springapp.mvc.data.VerificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by pradeep1 on 7/30/2014.
 */
@Service
public class VerificationService {

    @Autowired
    private VerificationRepository verificationRepository;

    private String generateVerificationString() {
        Random random = new Random();
        int min = 1000;
        int max = 9999;
        int randomNumber = random.nextInt(max - min) + min;
        String verificationString = Integer.toString(randomNumber);
        System.out.println("generated string " + verificationString);
        return verificationString;
    }

    public void addVerificationString(UserInfo userInfo) {
        Verification verification = new Verification();
        verification.setPhoneInfo(userInfo.getPhoneInfo());
        verification.setVerificationString(generateVerificationString());
        verificationRepository.saveOrUpdate(verification);
    }

    public void sendVerificationMessage(UserInfo userInfo) {
        Verification verification = verificationRepository.get(userInfo.getPhoneInfo());
        System.out.println(verification.getVerificationString());
        //TODO: add methods  to send sms;

    }

    public boolean verifyVerificationString(Verification verification) {
        if (verification != null) {
            Verification verification1 = verificationRepository.get(verification.getPhoneInfo());
            return (verification1 != null && verification1.getVerificationString().equals(verification.getVerificationString()));
        }
        return false;
    }
}
