package com.springapp.mvc.Services;

import com.springapp.mvc.HelperClasses.UserAndContacts;
import com.springapp.mvc.Model.Friendship;
import com.springapp.mvc.Model.UserInfo;
import com.springapp.mvc.data.FriendshipRepository;
import com.springapp.mvc.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeep1 on 7/31/2014.
 */
@Service
public class ContactService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;


    public List<UserInfo> verifyContacts(UserAndContacts userAndContacts) {
        if (userAndContacts == null) {
            return null;
        }
        int currentUserIndex = userAndContacts.getUserContacts().indexOf(userAndContacts.getUserInfo());
        if (currentUserIndex != -1) {
            userAndContacts.getUserContacts().remove(currentUserIndex);
        }
        UserInfo userInfo = userAndContacts.getUserInfo();
        List<UserInfo> friends = userAndContacts.getUserContacts();
        if (userInfo == null || friends == null || friends.isEmpty()) {
            return null;
        }
        List<Friendship> friendships = new ArrayList<Friendship>();
        for (UserInfo friend : friends) {
            if (friend != null) {
                Friendship friendship = new Friendship();
                friendship.setUserPhoneInfo(userInfo.getPhoneInfo());
                friendship.setFriendPhoneInfo(friend.getPhoneInfo());
                friendships.add(friendship);
            }

        }
        friendshipRepository.batchSaveOrUpdate(friendships);
        //userRepository.batchSaveOrUpdate(friends);
        //userRepository.batchSaveOrUpdate(registeredContacts);
        return userRepository.verifyRegisteredUsers(friends);
    }


}
